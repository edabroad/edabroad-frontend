(function () {
  'use strict';

  angular
    .module('app.home')
    .controller('HomeCtrl', HomeCtrl);

  /* @ngInject */
  function HomeCtrl($state) {
    var _this = this;
    _this.showSignUp = false;
    _this.goToLogIn = goToLogIn;
    _this.signUp =  signUp;
    _this.goToCreateUser = goToCreateUser;

    function signUp() {
      _this.showSignUp = !_this.showSignUp;
    }

    function goToCreateUser(student) {
      if (student) {
        $state.go('student-signup');
      } else {
        $state.go('tutor-signup');
      }
    }

    function goToLogIn() {
      $state.go('login');
    }

  }

})();
