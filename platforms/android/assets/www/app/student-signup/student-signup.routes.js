(function () {
  'use strict';

  angular
    .module('app.student-signup')
    .config(studentRoutes);

  function studentRoutes($stateProvider) {
    $stateProvider
      .state('student-signup', {
        url: '/student-signup',
        templateUrl: 'app/student-signup/student-signup.html',
        controller: 'StudentSignupController',
        controllerAs: 'vm',
      })
  }

})();
