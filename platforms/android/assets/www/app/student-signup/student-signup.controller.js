(function () {
  'use strict';

  angular
    .module('app.student-signup')
    .controller('StudentSignupController', StudentSignupController);

  function StudentSignupController($state,
                                   $ionicPopup,
                                   CORE) {
    var _this = this;
    _this.countries = CORE.COUNTRIES;
    _this.goToHome = goToHome;
    _this.signUp = signUp;
    _this.isClicked = isClicked;
    _this.optionClicked = optionClicked;
    var clickedOption = [false, false]
    _this.slide1 = [{holder: 'Full Name', type: 'text', model: 'name', icon: 'ion-person-add'},
    {holder: 'E-mail', type: 'email', model: 'email', icon: 'ion-email'},
    {holder: 'Age', type: 'number', model: 'age', icon: 'ion-calculator'},
    {holder: 'Country of Citizenship', type: 'text', model: 'country', icon: 'ion-map'},
    {holder: 'Password', type: 'password', model: 'password1', icon: 'ion-key'},
    {holder: 'Confirm Password', type: 'password', model: 'password2', icon: 'ion-key'},
    ];
    _this.slide2 = [{holder: 'International Baccalaureate', type: 'text', model: 'ib', icon: 'ion-ios-information'},
    {holder: 'Current School Name', type: 'text', model: 'school', icon: 'ion-ios-home-outline'},
    {holder: 'G.P.A.', type: 'number', model: 'gpa', icon: 'ion-clipboard'},
    {holder: 'Where would you like to study?', type: 'text', model: 'region', icon: 'ion-university'},
    {holder: 'What would you like to study?', type: 'text', model: 'carreer', icon: 'ion-ios-bookmarks-outline'},
    ];

    function signUp() {
    //TODO: save info in server
      success();
    }

    function success() {
      message('User created', 'You are now ready to login and start learning!');
      $state.go('login')
    }

    function prepareJSON() {}

    function handleError() {
      message('Please Try Again', 'We were not able to create your user, please try again');
    }

    function goToHome() {
      $state.go('home');
    }

    function message(title, text) {
      $ionicPopup.alert({
        title: title,
        template: text,
        });
    }

    function isClicked(left) {
      if (left) {
        return clickedOption[0];
      }

      return clickedOption[1];
    }

    function optionClicked(left) {
      if (left) {
        clickedOption[0] = !clickedOption[0];
      } else {
        clickedOption[1] = !clickedOption[1];
      }
    }
  }

})();
