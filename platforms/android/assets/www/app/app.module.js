(function () {
  'use strict';

  angular
    .module('app', [
      'app.core',
      'app.home',
      'app.student-signup',
      'app.tutor-signup',
    ]);
})();
