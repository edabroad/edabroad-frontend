(function () {
  'use strict';

  angular
    .module('app.student-signup')
    .config(tutorRoutes);

  function tutorRoutes($stateProvider) {
    $stateProvider
      .state('tutor-signup', {
        url: '/tutor-signup',
        templateUrl: 'app/tutor-signup/tutor-signup.html',
        controller: 'TutorSignupController',
        controllerAs: 'vm',
      })
  }

})();
