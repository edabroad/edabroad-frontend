(function () {
  'use strict';

  angular
    .module('app.tutor-signup')
    .controller('TutorSignupController', TutorSignupController);

  function TutorSignupController($state,
    $ionicPopup,
    CORE) {
    var _this = this;
    _this.countries = CORE.COUNTRIES;
    _this.asignatures = CORE.ASIGNATURES;
    _this.goToHome = goToHome;
    _this.signUp = signUp;
    var clickedOption = [false, false]
    _this.slide1 = [{ holder: 'Full Name', type: 'text', model: 'name', icon: 'ion-person-add' },
      { holder: 'E-mail', type: 'email', model: 'email', icon: 'ion-email' },
      { holder: 'Age', type: 'number', model: 'age', icon: 'ion-calculator' },
      { holder: 'Country of Citizenship', type: 'text', model: 'country', icon: 'ion-map' },
      { holder: 'Password', type: 'password', model: 'password1', icon: 'ion-key' },
      { holder: 'Confirm Password', type: 'password', model: 'password2', icon: 'ion-key' },
    ];
    _this.slide2 = [{holder: 'Current School Name', type: 'text', model: 'school', icon: 'ion-ios-home-outline'},
      {holder: 'What do you study?', type: 'text', model: 'study', icon: 'ion-clipboard'},
      {holder: 'What did you study?', type: 'text', model: 'study', icon: 'ion-clipboard'},
      {holder: 'Where do you study?', type: 'text', model: 'region', icon: 'ion-university'},
      {holder: 'What are your qualifications?', type: 'text', model: 'asignature', icon: 'ion-ios-bookmarks-outline'},
    ];

    function signUp() {
      //TODO: save info in server
      success();
    }

    function success() {
      message('Request was sended', 'you have to wait the answer in your email');
      sendRequest();
      $state.go('main-page')
    }

    function prepareJSON() { }

    function handleError() {
      message('Please Try Again', 'We were not able to create your user, please try again');
    }

    function goToHome() {
      $state.go('home');
    }

    function message(title, text) {
      $ionicPopup.alert({
        title: title,
        template: text,
      });
    }
    function isClicked(left) {
      if (left) {
        return clickedOption[0];
      }

      return clickedOption[1];
    }

    function optionClicked(left) {
      if (left) {
        clickedOption[0] = !clickedOption[0];
      } else {
        clickedOption[1] = !clickedOption[1];
      }
    }
    function sendRequest() {
        if(window.plugins && window.plugins.emailComposer) {
            window.plugins.emailComposer.showEmailComposerWithCallback(function(result) {
                console.log("Response -> " + result);
            }, 
            "Feedback for your App", // Subject
            "Email sended succedfully",   // Body
            ["maurisho01@gmail.com"],    // To
            null,                    // CC
            null,                    // BCC
            false,                   // isHTML
            null,                    // Attachments
            null);                   // Attachment Data
        }
    }
  }

})();
