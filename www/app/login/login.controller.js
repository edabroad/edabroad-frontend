(function () {
  'use strict';

  angular
    .module('app.login')
    .controller('LoginController', LoginController);

  /* @ngInject */
  function LoginController($state,
                     $ionicPopup) {

    var _this = this;
    _this.logIn = logIn;
    _this.goToHome = goToHome;
    _this.clicked = false;
    var user;

    function logIn() {
      //TODO: get user from server
      _this.clicked = true;
      user = {name: 'Sebastian Piedra', email: 'spiedra@pernixlabs.com',
              age: '18', country: 'Costa Rica', passWord: 'Pernix123.', };
      handleSuccess();
    }

    function validateUser() {
    }

    function handleSuccess() {
      // _this.clicked =
      // sessionService.setCurrentUser(user);
      // $state.go('profile');
    }

    function goToHome() {
      $state.go('home');
    }
  }
})();
