(function () {
  'use strict';

  angular
    .module('app.student-signup')
    .controller('StudentSignupController', StudentSignupController);

  function StudentSignupController($state,
                                   $ionicPopup,
                                   CORE) {
    var _this = this;
    _this.countries = CORE.COUNTRIES;
    _this.goToHome = goToHome;
    _this.signUp = signUp;
    _this.user = {};
    _this.user.clickedOptions = [false, false, false, false];
    _this.optionClicked = optionClicked;
    _this.slide1 = [{holder: 'Full Name', type: 'text', model: 'name', icon: 'ion-person-add'},
      {holder: 'E-mail', type: 'email', model: 'email', icon: 'ion-email'},
      {holder: 'Age', type: 'number', model: 'age', icon: 'ion-calculator'},
      {holder: 'Password', type: 'password', model: 'password1', icon: 'ion-key'},
      {holder: 'Confirm Password', type: 'password', model: 'password2', icon: 'ion-key'},
    ];
    _this.slide2 = [ {holder: 'Current School Name', type: 'text', model: 'school', icon: 'ion-ios-home-outline'},
      {holder: 'G.P.A.', type: 'number', model: 'gpa', icon: 'ion-clipboard'},
      {holder: 'Where would you like to study?', type: 'text', options: CORE.REGIONS, model: 'region', icon: 'ion-university'},
      {holder: 'Area of interest?', type: 'text', model: 'career', icon: 'ion-ios-bookmarks-outline'},
    ];
    _this.slide3 = [ {holder: 'International Baccalaureate', options: ['Yes', 'No'], model: 'ib', icon: 'ion-ios-information'},
      {holder: 'What are you looking for?', options: ['Help with school', 'Help applying abroad'], model: 'help', icon: 'ion-ios-paw-outline'},
      {holder: 'Country', type: 'text', model: 'country', icon: 'ion-map'},
    ];

    function signUp() {
      if (_this.user && Object.keys(_this.user).length == 11) {
        if (_this.user.password1 == _this.user.password2) {
          validInfo();
        } else {
          message('Different Passwords', 'The passwords do not match')
        }
      } else if (_this.user && !_this.user.email){
        message('Invalid E-mail', 'Please write an actual email');
      } else {
        message('Incomplete Info', 'Please fill all the info');
      }
    }

    function validInfo() {
      //TODO: SAVE INFO ON THE SERVER
      message('User created', 'You are now ready to login and start learning!');
      $state.go('home')
    }

    function handleError() {
      message('Please Try Again', 'We were not able to create your user, please try again');
    }

    function goToHome() {
      $state.go('home');
    }

    function message(title, text) {
      $ionicPopup.alert({
        title: title,
        template: text,
        });
    }

    function optionClicked(field, button) {
      _this.user.clickedOptions[field * 2 + button] = !_this.user.clickedOptions[field * 2 + button];
    }
  }

})();
