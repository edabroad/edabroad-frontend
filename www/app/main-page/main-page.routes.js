(function () {
  'use strict';

  angular
    .module('app.main-page')
    .config(mainPageRoutes);

  function mainPageRoutes($stateProvider) {
    $stateProvider
      .state('main-page', {
        url: '/main-page',
        templateUrl: 'app/main-page/main-page.html',
        controller: 'MainPageController',
        controllerAs: 'vm',
      });
  }

})();